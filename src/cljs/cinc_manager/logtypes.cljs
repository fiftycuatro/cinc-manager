(ns cinc-manager.logtypes
  (:use [cinc-manager.interop :only [re-toString]])
  (:require [reagent.core :as r]
            [reagent-modals.modals :as modals]
            [cinc-manager.controls :as c]
            [cinc-manager.session :as session]
            [cinc-manager.transit :as transit]))
(def logtype-what-options [{:key :previous :display "Previous"}
                           {:key :next :display "Next"}])

(defn append-logtype [logtype]
  (let [logtypes (session/get :logtypes)]
    (session/put! :logtypes (sort-by #(str (.toLowerCase (:name %))) (conj logtypes logtype)))))

(defn logtypes-table [data]
  [:table {:class "table table-striped table-bordered"}
   [:thead
    [:tr
     [:th "Name"]
     [:th "Event Pattern"]
     [:th "Timestamp Pattern"]
     [:th "What"]
     [:th "Negate?"]]]
   [:tbody
    (for [logtype data]
      ^{:key logtype} [:tr
                      [:td (:name logtype)]
                      [:td (re-toString (:event-pattern logtype))]
                      [:td (re-toString (:timestamp-pattern logtype))]
                      [:td (name (:what? logtype))]
                      [:td (.toString (:negate? logtype))]])]])

(defn logtype-add-success [logtype]
  (append-logtype logtype)
  (c/message-modal! [:div
                     [:h3 "Successfully Added Log Type"]
                     [:i {:class "fa fa-check-circle-o fa-5x success"} ""]]))


(defn logtype-add-error [error]
  (let [message (:error-message error)]
    (c/message-modal! [:div
                       [:h2 message]
                       [:i {:class "fa fa-times-circle-o fa-5x error"} ""]])))

(defn add-logtype-form [logtype]
    [:form
     (c/with-label "Name"
       [c/input-element "name" "name" "text" [:name] logtype])
     (c/with-label "Event Pattern"
       [c/input-element "event-pattern" "event-pattern" "text" [:event-pattern] logtype re-toString re-pattern])
     (c/with-label "Timestamp Pattern"
       [c/input-element "timestamp-pattern" "timestamp-pattern" "text" [:timestamp-pattern] logtype re-toString re-pattern])
     (c/with-label "What"
       [:select.form-control {:on-change (fn [e] (swap! logtype update-in [:what?] #(-> e .-target .-value keyword)))}
        (for [option logtype-what-options]
          ^{:key option} [:option {:value (:key option)} (:display option)])])
     (c/with-label "Negate?"
       [c/checkbox "negate" "negate" [:negate?] logtype])
     ])

(defn add-logtype-modal []
  (let [logtype (r/atom {:name "" :event-pattern "" :timestamp-pattern "" :what? :previous :negate? false})]
    (fn []
      [:div
       [:div.modal-header
        [:h3 "Add Log Set"]]
       [:div.modal-body
        [add-logtype-form logtype]]
       [:div.modal-footer
        [:div
         [:button.btn.btn-primary
          {:on-click
           (fn []
             (let [url (str "logtypes/" (:name @logtype))]
               (transit/send-data url @logtype logtype-add-success logtype-add-error))) }
          "Add"]
         [:button.btn.btn-default
          {:on-click #(modals/close-modal!)}
          "Cancel"]]]])))

(defn add-logset-button []
  [:button.btn.btn-primary.pull-right
   {:on-click #(modals/modal! [(add-logtype-modal)])}
   "Add Log Set"])

(defn logtypes-page [data]
  (logtypes-table data))

(defn get-logtypes []
  (transit/get-data "logtypes" (fn [logtypes]
                                (session/put! :logtypes logtypes))))

(defn page []
  (r/create-class
    {:component-did-mount get-logtypes
     :reagent-render (fn []
                       [:div {:class "container"}
                        [modals/modal-window]
                        [logtypes-page (session/get :logtypes)]
                        [add-logset-button]])}))

