(ns cinc-manager.transit
  (:use [cinc-manager.interop :only [re-toString]])
  (:require [cognitect.transit :as transit]
            [cljs-time.coerce :as c])
  (:import [goog.net XhrIo]))

;; Convert from standard javascript types
;;
(defn- isDate? [d]
  (= (-> js/Object .-prototype .-toString (.call d)) "[object Date]"))

(defn- convert-all [m matches convert]
  (let [f (fn [[k v]]
            (if (matches v)
              [k (convert v)]
              [k v]))]
    (clojure.walk/postwalk (fn [x] (if (map? x) (into {} (map f x)) x)) m)))

(defn- date->datetimes [m]
  (convert-all m #(isDate? %) c/to-date-time))

(defn- datetimes->date [m]
  (convert-all m #(instance? goog.date.Date %) c/to-date))

;; Transit Writers and Readers
;;
(def pattern-writer
  (transit/write-handler
    (constantly "p")
    #(-> % re-toString)))

(def pattern-reader
  (transit/read-handler
    (fn [p]
      (re-pattern p))))

(defn- read-transit [transit-json]
  (let [r (transit/reader :json {:handlers {"p" pattern-reader}})]
    (-> (transit/read r transit-json)
        date->datetimes)))

(defn- write-transit [data]
  (let [w (transit/writer :json {:handlers {js/RegExp pattern-writer}})]
    (->> data
         datetimes->date
         (transit/write w))))

(defn get-data [url cb]
  (XhrIo.send (str "/" url)
              (fn [e]
                (let [xhr (.-target e)
                      response-data (-> xhr .getResponseText read-transit)]
                  (cb response-data)))))


(defn- success? [status]
  (and (<= status 200) (<= status 299)))

(defn send-data
  ([url] (send-data url {}))
  ([url data] (send-data url data #()))
  ([url data success-cb] (send-data url data success-cb #()))
  ([url data success-cb error-cb]
   (XhrIo.send (str "/" url)
               (fn [e]
                 (let [xhr (.-target e)
                       response-data (-> xhr .getResponseText read-transit)
                       status (.getStatus xhr)]
                   (if (success? status)
                     (success-cb response-data)
                     (error-cb response-data))))
               "POST"
               (write-transit data)
               {"Content-Type" "application/transit+json"})))

