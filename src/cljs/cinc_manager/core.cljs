(ns cinc-manager.core
  (:import goog.History)
  (:require [reagent.core :as r]
            [devcards.core]
            [secretary.core :as secretary]
            [goog.events :as events]
            [goog.history.EventType :as EventType]
            [cinc-manager.session :as session]
            [cinc-manager.logsets :as logsets]
            [cinc-manager.logtypes :as logtypes])
  (:require-macros [devcards.core :as dc :refer [defcard defcard-rg defcard-doc]]
                   [secretary.core :as secretary :refer [defroute]]))

(defn hook-google-history []
  (let [h (History.)]
    (events/listen h EventType/NAVIGATE #(secretary/dispatch! (.-token %)))
    (doto h (.setEnabled true))))



(defn navbar-header []
  [:div.navbar-header
   [:button.navbar-toggle {:type "button"
                           :data-toggle "collapse" :data-target "#navbar-collapse1"}
    [:span.icon-bar]
    [:span.icon-bar]
    [:span.icon-bar]]
   [:a.navbar-brand {:href "#"}
    [:img {:src "/img/cinc_logo_nav_branding.png"}]]])

(defn navbar-links [links]
  [:div {:class "collapse navbar-collapse" :id "navbar-collapse1"}
    [:ul {:class "nav navbar-nav"}
     (for [link links]
       ^{:key link} [:li [:a {:href (:url link)} (:display link)]])]])

(defn navbar [links]
  [:nav {:class "navbar navbar-default"}
   (navbar-header)
   (navbar-links links)])

(defcard navbar-card
         (r/as-element [navbar [{:display "Log Sets" :url "#"}
                                {:display "Log Types" :url "#"}]]))

(def links [{:display "Log Sets" :url "#logsets"}
            {:display "Log Types" :url "#logtypes"}])

(defn page []
  [:div.body
   [navbar links]
   [(session/get :current-page)]])

(defroute "/" []
          (session/put! :current-page logsets/page))
(defroute "/logsets" []
          (session/put! :current-page logsets/page))
(defroute "/logtypes" []
          (session/put! :current-page logtypes/page))

(defn init! []
  ;; conditionally start the app based on the presence of #main-app-area
  ;; node is on the page
  (if-let [node (.getElementById js/document "main-app-area")]
    (do
      (hook-google-history)
      (secretary/set-config! :prefix "#")
      (session/put! :navlinks links)
      (session/put! :logsets [])
      (r/render-component
        [page]
        (.getElementById js/document "main-app-area")))))

(init!)