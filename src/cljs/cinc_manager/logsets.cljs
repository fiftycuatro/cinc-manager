(ns cinc-manager.logsets
  (:require [reagent.core :as r]
            [devcards.core :as dc :refer-macros [defcard defcard-rg defcard-doc]]
            [cljs-time.core :as t]
            [cljs-time.format :as f]
            [cinc-manager.controls :as c]
            [reagent-modals.modals :as r-modals]
            [cinc-manager.session :as session]
            [cinc-manager.transit :as transit]))

(def date-format (f/formatters :date-hour-minute))

(defn format-date [date]
  (f/unparse date-format date))

(defn append-logset [logset]
  (let [logsets (session/get :logsets)]
    (session/put! :logsets (sort-by #(str (.toLowerCase (:name %))) (conj logsets logset)))))

(defn add-logset [logset]
  (let [url (str "logsets/" (:name logset))]
    (transit/send-data url
                       logset
                       (fn [_]
                         (append-logset logset)
                         (c/success-message-modal! "Successfully Added Log Set"))
                       (fn [resp]
                         (c/error-message-modal! (:error-message resp))))))

(defn processed-label [logset]
  (let [{:keys [processed? processed-count]} logset]
    (cond
      processed? [:span {:class "label label-success"} (str "Processed " processed-count " Events")]
      (> processed-count 0) [:span {:class "label label-warning"} (str "Processed " processed-count " Events")]
      :else [:span {:class "label label-default"} "Not Processed"])))

(defn unload-button? [logset]
  (let [{:keys [processed? name]} logset]
    (when processed?
      [:button.btn.btn-danger {:on-click #(transit/send-data (str "logsets/unload/" name))} "Unload"])))

(defn processed-button? [logset]
  (let [{:keys [processed? processed-count name]} logset]
    (when (and (not processed?) (= processed-count 0))
      [:button.btn.btn-primary {:on-click #(transit/send-data (str "logsets/process/" name))} "Process"])))

(defn processing-spinner? [logset]
  (let [{:keys [processed? processed-count]} logset]
    (when (and (not processed?) (> processed-count 0))
      [:i {:class "fa fa-spinner fa-2x fa-spin"} ""])))

(defn logsets-table [data]
  [:table {:class "table table-striped table-bordered"}
   [:thead
    [:tr
     [:th "Name"]
     [:th "Start Time"]
     [:th "End Time"]
     [:th]
     [:th]]]
   [:tbody
    (for [logset data]
      ^{:key logset} [:tr
                      [:td (:name logset)]
                      [:td (format-date (:start-time logset))]
                      [:td (format-date (:end-time logset))]
                      [:td (processed-label logset)]
                      [:td
                       (processed-button? logset)
                       (unload-button? logset)
                       (processing-spinner? logset)]
                      ])]])

(defn add-logset-form [logset]
  (let [date-fmt "YYYY-MM-dd'T'HH:mm"]
    [:form
     (c/with-label "Name"
       [c/input-element "name" "name" "text" [:name] logset])
     (c/with-label "Start Time"
       [c/datepicker-input "start" "start" [:start-time] logset date-fmt])
     (c/with-label "End Time"
       [c/datepicker-input "end" "end" [:end-time] logset date-fmt])]))

(defn add-logset-modal []
  (let [logset (r/atom {:name "" :start-time (t/now) :end-time (t/now)})]
    (fn []
    [:div
     [:div.modal-header
      [:h3 "Add Log Set"]]
     [:div.modal-body
      [add-logset-form logset]]
     [:div.modal-footer
      [:div
       [:button.btn.btn-primary {:on-click #(add-logset @logset)} "Add"]
       [:button.btn.btn-default {:on-click #(r-modals/close-modal!)} "Cancel"]]]])))

(defn add-logset-button []
  [:button.btn.btn-primary.pull-right
   {:on-click #(r-modals/modal! [(add-logset-modal)])}
   "Add Log Set"])

(defn logsets-page [logsets]
  [:div
   [:h1 "LOG SETS"]
   [logsets-table logsets]])

(defn get-logsets []
  (transit/get-data "logsets" (fn [logsets]
                                 (session/put! :logsets logsets))))

(defn page []
    (r/create-class
      {:component-did-mount
       (fn []
         (session/put! :logset-refresh-subscription (.setInterval js/window #(get-logsets) 1000))
         (get-logsets))
       :component-will-unmount
       (fn []
         (.clearInterval js/window (session/get :logset-refresh-subscription)))
       :reagent-render
       (fn []
         [:div {:class "container"}
          [r-modals/modal-window]
          [logsets-page (session/get :logsets)]
          [add-logset-button]])}))


(defcard logset-table-card
         (r/as-element [logsets-table [{:name "unprocessed" :start-time (t/date-time 2014 12 10) :end-time (t/date-time 2012 10 2) :processed? false :processed-count 0}
                                       {:name "prococessing" :start-time (t/date-time 2014 12 10) :end-time (t/date-time 2012 10 2) :processed? false :processed-count 10}
                                       {:name "processed" :start-time (t/date-time 2014 12 10) :end-time (t/date-time 2012 10 2) :processed? true :processed-count 1000}
                                       ]]))

(defcard-rg add-logset-form
            add-logset-form
            (r/atom {:name "" :start-time (t/now) :end-time (t/now)})
            {:inspect-data true})


