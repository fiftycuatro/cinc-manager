(ns cinc-manager.interop)

;; Functions that facilitate some of the differences between Clojure and Clojurescript

(defn- remove-empty-re-sign [s]
  (if (.startsWith s "(?:)")
    (subs s 4)
    s))

(defn re-toString
  "removes the leading and following slashes('/') in a RegExp toString"
  [pattern]
  (let [regExp-string (.toString pattern)
        end (dec (count regExp-string))]
    (remove-empty-re-sign (subs regExp-string 1 end))))