(ns cinc-manager.controls
  (:require [reagent.core :as r]
            [cljs-time.format :as f]
            [reagent-modals.modals :as modals]))

(defn with-label [label input]
  [:div.form-group
   [:label.control-label {:for name} label]
   input])

(defn checkbox
  ([id name path state] (checkbox id name path state identity identity))
  ([id name path state in-fmt out-fmt]
   [:input.form-control {:id id
                         :name name
                         :type "checkbox"
                         :value (in-fmt (get-in @state path))
                         :on-change (fn [element]
                                      (swap! state update-in path #(-> element .-target .-checked out-fmt)))}]))

(defn input-element
  ([id name type path state] (input-element id name type path state identity identity))
  ([id name type path state in-fmt out-fmt]
   [:input.form-control {:id id
                         :name name
                         :type type
                         :value (in-fmt (get-in @state path))
                         :on-change (fn [element]
                                      (swap! state update-in path #(-> element .-target .-value out-fmt)))}]))

(defn- cleanup-format [fmt-str]
  (let [mappings {"'" ""
                  "d" "D"
                  "D" "d"}]
  (clojure.string/join
    (map #(or (get mappings %) %) fmt-str))))

(defn- parse-date [date-string format]
  (f/parse format date-string))

(defn datepicker-input [id name path state fmt-str]
    (r/create-class
      {:reagent-render
       (fn [id name path state fmt-str]
         [:div.input-group.date.dateTimes
          [:input.form-control {:id id
                                :name name
                                :type type
                                :defaultValue (f/unparse (f/formatter fmt-str) (get-in @state path))
                                }]
          [:span.input-group-addon
           [:span.glyphicon.glyphicon-calendar ""]]])

       :component-did-mount
       #(do
         ;; initialize datetimepicker on element
         (.datetimepicker
           (-> % r/dom-node js/$ .children .first)
           (clj->js {:format (cleanup-format fmt-str)}))
         ;; update function for datetimepicker
         (.on
           (js/$ (r/dom-node %))
           "dp.change"
           (fn [e]
             (swap! state update-in path
                    (fn [_]
                      (-> e .-target .-value (parse-date (f/formatter fmt-str))))))))}))


(defn message-modal!
  ([body] (message-modal! body nil))
  ([body title]
   (modals/modal!
     [:div
      (if-not (nil? title)
        [:div.modal-header
         [:h3 title]])
      [:div.modal-body
       [:div {:class "text-center"}
        body]]
      [:div.modal-footer
       [:button.btn.btn-default {:on-click #(modals/close-modal!)} "Close"]]])))

(defn success-message-modal!
  ([message] (success-message-modal! message nil))
  ([message title]
  (message-modal! [:div
                     [:h3 message]
                     [:i {:class "fa fa-check-circle-o fa-5x success"} ""]]
                  title)))

(defn error-message-modal!
  ([message] (error-message-modal! message nil))
  ([message title]
  (message-modal! [:div
                     [:h2 message]
                     [:i {:class "fa fa-times-circle-o fa-5x error"} ""]]
                  title)))
