(ns cinc-manager.activemq
  (:require [clojure.core.async :refer [go-loop <!]]
            [clamq.protocol.connection :as clamq]
            [clamq.activemq :as activemq]
            [clamq.protocol.producer :as producer]
            [cinc-manager.shipper :as shipper]
            [com.stuartsierra.component :as component]
            [clojure.tools.logging :as log]))

(defn- run-shipper-loop [conn channels dest]
  (let [producer (clamq/producer conn)]
    (go-loop []
      (if-let [event (<! (shipper/receive-event channels))]
        (do
          (producer/publish producer dest event)
          (recur))
        (log/debug "Exiting activemq go loop!!")))))

(defrecord ActivemqShipper [broker-url broker-dest]
  component/Lifecycle
  (start [component]
    (log/info "Starting Activemq Shipper | " {:broker-url broker-url :broker-dest broker-dest})
    (let [{:keys [shipper-channels]} component
          conn (activemq/activemq-connection broker-url)]
      (run-shipper-loop conn shipper-channels broker-dest)
      (assoc component :activemq-conn conn)))
  (stop [component]
    (log/info "Stopping Activemq Shipper")
    (if-let [activemq-conn (:activemq-conn component)]
      (do
        (clamq/close activemq-conn)
        (dissoc component :activemq-conn))
      component)))

(defn new-activemq-shipper [broker-url broker-dest]
  (ActivemqShipper. broker-url broker-dest))

