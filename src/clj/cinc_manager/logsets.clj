(ns cinc-manager.logsets
  (:require [clojure.java.io :refer [file]]
            [clj-time.format :as f]
            [clojure.string :as string]
            [cinc-manager.io :as cincio]
            [cinc-manager.shipper :as shipper]
            [cinc-manager.elasticsearch :as es]))

(defrecord Logsets [logsets-path logtype-config-path elasticsearch-url])

(defn new-logsets [logsets-path logtype-config-path elasticsearch-url]
  (Logsets. logsets-path logtype-config-path elasticsearch-url))


;; Elasticsearch methods

(defn logset-index-name [logset-name]
  (str "logstash-cinc-" (.toLowerCase logset-name)))

(defn logset-processed? [logsets logset-name]
  (let [{:keys [elasticsearch-url]} logsets
        index-name (logset-index-name logset-name)
        results (es/search-doc-type elasticsearch-url index-name "cinc-process-complete")
        doc-count (or (get-in results ["hits" "total"]) 0)]
    (pos? doc-count)))

(defn unload-logset [logsets logset-name]
  (let [{:keys [elasticsearch-url]} logsets
        index-name (logset-index-name logset-name)]
    (es/delete-index elasticsearch-url index-name)))

(defn event-count [logsets logset-name]
  (let [{:keys [elasticsearch-url]} logsets
        index-name (logset-index-name logset-name)]
    (es/index-doc-count elasticsearch-url index-name)))

;; Logset
(defrecord Logset [path name start-time end-time processed? processed-count])

(def date-format (f/formatter "yyyy-MM-dd'T'HHmm"))

(defn create-logset [logsets path]
  (let [log-set-dir-name (.getName (file path))
        split (string/split log-set-dir-name #"_")
        name (nth split 0)
        start-time (f/parse date-format (nth split 1))
        end-time (f/parse date-format (nth split 2))
        processed? (logset-processed? logsets name)
        processed-count (event-count logsets name)]
    (Logset. path name start-time end-time processed? processed-count)))

(defn get-logsets [logsets]
  (let [{:keys [logsets-path]} logsets
        directories (filter #(.isDirectory %) (.listFiles (file logsets-path)))]
    (map #(create-logset logsets (.getAbsolutePath %)) directories)))

(defn logset-byname [logsets name]
  (let [{:keys [logsets-path]} logsets
        all-logsets (get-logsets logsets)]
    (first (filter #(.equalsIgnoreCase (:name %) name) all-logsets))))

(defn add-logset [logsets name start-time end-time]
  (let [{:keys [logsets-path]} logsets]
    (if (logset-byname logsets name)
      (throw (Exception. (str "Logset with name " name " already exists.")))
      (let [start-time-str (f/unparse date-format start-time)
            end-time-str (f/unparse date-format end-time)
            folder-name (str name "_" start-time-str "_" end-time-str)
            log-set-directory (file (file logsets-path) folder-name)]
        (.mkdir log-set-directory)
        (create-logset logsets (.getAbsolutePath log-set-directory))))))

;; Logtype
(defrecord Logtype [name event-pattern timestamp-pattern what? negate?])

(defn get-logtypes [logsets]
  (let [{:keys [logtype-config-path]} logsets]
    (if (.exists (file logtype-config-path))
      (cincio/read-from-file logtype-config-path)
      [])))

(defn add-logtype [logsets logtype]
  (let [{:keys [logtype-config-path]} logsets
        types (get-logtypes logsets)]
    (cincio/write-to-file logtype-config-path (conj types logtype))))

;; Event Parsing
(defn- first-match
  "Returns string up until the first match, the match and the remaining sequence"
  ([pattern seq negate?] (first-match pattern seq negate? ""))
  ([pattern seq negate? so-far]
   (if-let [line (first seq)]
     (let [match (re-find pattern line)]
       (if (or (and match (not negate?)) (and (not match) negate?))
         (first-match pattern (rest seq) negate? (str so-far \newline line))
         {:up-to so-far :match line :seq (rest seq)}))
     {:remainder so-far})))

(defn- matches [pattern xs options]
  (let [{:keys [what? negate?]} options]
    (letfn [(helper [s current-match]
              (lazy-seq
                (let [{:keys [up-to match seq remainder]} (first-match pattern s negate?)]
                  (if (nil? remainder)
                    (if (= what? :previous)
                      (cons (str current-match up-to) (helper seq match))
                      (cons (str up-to \newline match) (helper seq "")))
                    (cons (str current-match remainder) nil)))))]
      (helper xs ""))))

(defn process-events-in-file
  ([pattern file-path f] (process-events-in-file pattern file-path f {}))
  ([pattern file-path f options]
   (let [default-options {:what? :previous :negate? true}
         final-options (merge default-options options)
         raw-lines (cincio/lazy-file-lines file-path)
         events (filter #(not (clojure.string/blank? %)) (matches pattern raw-lines final-options))]
     (doseq [event events]
       (let [filename (.getName file-path)]
         (f (assoc {} :message event
                      :filename filename)))))))

(defn process-logtype-events [logtype logset fn]
  (let [files (cincio/files-in (file (file (:path logset)) (:name logtype)))
        event-pattern (:event-pattern logtype)
        options (select-keys logtype [:what? :negate?])]
    (doseq [file files]
      (process-events-in-file event-pattern file fn options))))

(defn process-logset-events [logsets logset-name channels]
  (let [logset (logset-byname logsets logset-name)
        logtypes (get-logtypes logsets)
        logset-name (.toLowerCase logset-name)]
    (future
      (doseq [logtype logtypes]
        (process-logtype-events logtype logset #(shipper/ship-event channels (-> %
                                                                                 (assoc :type (:name logtype))
                                                                                 (assoc :logset logset-name)))))
      (shipper/ship-event channels {:type "cinc-process-complete" :logset logset-name}))))

