(ns cinc-manager.system
  (:require [com.stuartsierra.component :as component]
            [cinc-manager.server :as server]
            [cinc-manager.shipper :as shipper]
            [cinc-manager.activemq :as activemq-shipper]
            [cinc-manager.logsets :as logsets]))

(defn dev-system [options]
  (let [{:keys [port logsets-path logtype-config-path broker-dest broker-url elasticsearch-url]} options]
    (component/system-map
      :logsets (logsets/new-logsets logsets-path logtype-config-path elasticsearch-url)
      :shipper-channels (shipper/new-shipper-channels)
      :activemq-shipper (component/using (activemq-shipper/new-activemq-shipper broker-url broker-dest) [:shipper-channels])
      :server (server/dev-server port))))

