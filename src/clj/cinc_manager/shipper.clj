(ns cinc-manager.shipper
  (:require [clojure.core.async :refer [chan close! <! >!! go]]
            [cheshire.core :as json]
            [com.stuartsierra.component :as component]))

(defrecord ShipperChannels []
  component/Lifecycle
  (start [component]
    (let [c (chan)]
      (assoc component :shipper-channel c)))
  (stop [component]
    (if-let [shipper-channel (:shipper-channel component)]
      (do
        (close! shipper-channel)
        (dissoc component :shipper-channel))
      component)))

(defn new-shipper-channels []
  (ShipperChannels.))

(defn ship-event [channels event]
  (let [{:keys [shipper-channel]} channels]
    (>!! shipper-channel (json/generate-string event))))

(defn receive-event [channels]
  (let [{:keys [shipper-channel]} channels]
    (go (<! shipper-channel))))

