(ns cinc-manager.core
  (:require [com.stuartsierra.component :as component]
            [clojure.string :as string]
            [clojure.tools.cli :refer [parse-opts]]
            [cinc-manager.system :as system])
  (:gen-class))

;; ===================================================
;; Main

(def required-opts #{:logsets-path :logtype-config-path :broker-url :broker-dest :elasticsearch-url})

(defn missing-required? [opts]
  (not-every? opts required-opts))

(def cli-options
  [["-p" "--port PORT" "Port number"
    :default 8081
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]
   ["-f" "--logsets-path PATH_TO_LOGSETS" "Logset Base Path"
     :validate [#(.exists (clojure.java.io/file %)) "Must be an existing directory"
                #(.isDirectory (clojure.java.io/file %)) "Must be a directory"]]
   ["-t" "--logtype-config-path PATH_TO_LOGTYPE_CONFIG" "Logtype configuration path"]
   [nil "--broker-url BROKER_URL" "Activemq Broker Url"]
   [nil "--broker-dest BROKER_DESTINATION" "Activemq Destination"]
   [nil "--elasticsearch-url ELASTICSEARCH_HTTP_URL" "Elasticsearch HTTP URL (usually http://hostname:9200)"]
   ["-h" "--help"]])

(defn usage [options-summary]
  (->> ["CINC Manager"
        ""
        "Usage: program-name [options]"
        ""
        "Options:"
        options-summary]
       (string/join \newline)))

(defn required-error-msg [opts]
  (str "The following options are required:\n\n"
       (string/join \newline (clojure.set/difference required-opts opts))))

(defn error-msg [errors]
  (str "The following errors occured while parsing your command:\n\n"
       (string/join \newline errors)))

(defn exit [status msg]
  (println msg)
  (System/exit status))

(defn -main [& args]
  (let [{:keys [options errors summary]} (parse-opts args cli-options)]
    (cond
      (:help options) (exit 0 (usage summary))
      (missing-required? options) (exit 0 (required-error-msg options))
      errors (exit 1 (error-msg errors)))
      :default (component/start (system/dev-system options))))
