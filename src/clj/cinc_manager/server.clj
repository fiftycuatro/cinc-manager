(ns cinc-manager.server
  (:use compojure.core
        ring.util.response)
  (:require [ring.adapter.jetty :as jetty]
            [ring.util.response :as resp]
            [compojure.handler :as handler]
            [compojure.route :as route]
            [clj-time.coerce :as c]
            [cognitect.transit :as transit]
            [ring.middleware.transit :refer [wrap-transit-response wrap-transit-body]]
            [cinc-manager.logsets :as logsets]
            [com.stuartsierra.component :as component]
            [cheshire.generate :refer [add-encoder encode-str]]
            [clojure.tools.logging :as log]))

(defn convert-all [m matches convert]
  (let [f (fn [[k v]]
            (if (matches v)
              [k (convert v)]
              [k v]))]
    (clojure.walk/postwalk (fn [x] (if (map? x) (into {} (map f x)) x)) m)))

(defn date->datetime [m]
  (convert-all m #(instance? java.util.Date %) c/from-date))

(add-encoder org.joda.time.DateTime
             (fn [date jsonGenerator]
               (.writeString jsonGenerator (c/to-string date))))

(def joda-time-writer
  (transit/write-handler
    (constantly "m")
    #(-> % c/to-date .getTime)
    #(-> % c/to-date .getTime .toString)))

(def pattern-writer
  (transit/write-handler
    (constantly "p")
    #(-> % .toString)))

(def pattern-reader
  (transit/read-handler
    (fn [p]
      (re-pattern p))))

(defn add-logtype [logsets params]
  (let [{:keys [name event-pattern timestamp-pattern what? negate?]} params
        logtype (logsets/->Logtype name event-pattern timestamp-pattern what? negate?)]
    (logsets/add-logtype logsets logtype)
    logtype))

(defroutes app
           (GET "/logsets" {logsets :logsets} (response (logsets/get-logsets logsets)))
           (POST "/logsets/:name" {{name :name start-time :start-time end-time :end-time} :body
                                   logsets :logsets}
             (response (logsets/add-logset logsets name start-time end-time)))
           (POST "/logsets/process/:name" {params :params
                                           logsets :logsets
                                           shipper-channels :shipper-channels}
             (logsets/process-logset-events logsets (:name params) shipper-channels)
             (response {:message "OK"}))
           (POST "/logsets/unload/:name" {params :params
                                          logsets :logsets}
             (if (logsets/unload-logset logsets (:name params))
               (response {:message "Sucessfully unloaded logset"})
               {:status 500 :body {:error_messge "Could not unload logset"}}))
           (GET "/logtypes" {logsets :logsets} (response (logsets/get-logtypes logsets)))
           (POST "/logtypes/:name" {body :body
                                    logsets :logsets}
             (do
               (println logsets)
               (response (add-logtype logsets body))))
           (GET "/" []
                (resp/content-type (resp/resource-response "index.html" {:root "public"}) "text/html"))
           (route/resources "/")
           (route/not-found "Page not found"))


(defn wrap-shipper [handler shipper-channels]
  (fn [req]
    (handler (assoc req :shipper-channels shipper-channels))))

(defn wrap-logsets [handler logsets]
    (fn [req]
      (handler (assoc req :logsets logsets))))

(defn wrap-exceptions [handler]
  (fn [req]
    (try (handler req)
         (catch Exception e
           (.printStackTrace e)
           {:status 500
            :body {:error-message (.getMessage e)}}))))

(defn wrap-body-dates [handler]
  (fn [req]
    (handler (update-in req [:body] #(date->datetime %)))))

(defn cinc-handler [logsets shipper-channels]
  (-> (handler/site app)
      (wrap-logsets logsets)
      (wrap-shipper shipper-channels)
      wrap-exceptions
      (wrap-transit-response {:encoding :json :opts {:handlers {org.joda.time.DateTime joda-time-writer
                                                                java.util.regex.Pattern pattern-writer}}})
      wrap-body-dates
      (wrap-transit-body {:keywords? true :opts {:handlers {"p" pattern-reader}}})))

;; =============================================================================
;; WebServer

(defrecord WebServer [port handler]
  component/Lifecycle
  (start [component]
    (log/infof "Starting WebServer on port %s" port)
    (let [{:keys [logsets shipper-channels]} component
          req-handler (handler logsets  shipper-channels)
          container (jetty/run-jetty req-handler
                                     {:port port :join? false})]
      (assoc component :container container)))
  (stop [component]
    (log/info "Stopping WebServer")
    (if-let [container (:container component)]
      (do
        (.stop container)
        (dissoc component :container))
      component)))

(defn dev-server [web-port]
  (component/using
     (WebServer. web-port cinc-handler)
     [:logsets :shipper-channels]))

