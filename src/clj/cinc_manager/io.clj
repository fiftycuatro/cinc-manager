(ns cinc-manager.io
  (:require [clojure.java.io :refer [file reader]]))

(defn lazy-file-lines
  "Returns a lazy sequence of lines from file or file path. NOTE: File is not closed until entire sequence is read."
  [file]
  (letfn [(helper [rdr]
            (lazy-seq
              (if-let [line (.readLine rdr)]
                (cons line (helper rdr))
                (do (.close rdr) nil))))]
    (helper (reader file))))

(defn read-from-file
  "Reads one object from the file located at the path."
  [path]
  (read-string (slurp path)))

(defn write-to-file
  "Writes data object to the file located at the path."
  [path data]
  (spit path (with-out-str (pr data))))

(defn files-in
  "Returns all files that are files in a directory path."
  [dir]
  (filter #(.isFile %) (file-seq dir)))

