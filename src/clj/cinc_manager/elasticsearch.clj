(ns cinc-manager.elasticsearch
  (:require [clojure.string :as string]
            [clj-http.client :as http]
            [cheshire.core :as json]
            [clojure.tools.logging :as log]))

(defn indices-response->map [line]
  (let [parts (string/split line #"\s")]
    (assoc {} :health (nth parts 0)
              :status (nth parts 1)
              :index (nth parts 2)
              :pri (nth parts 3)
              :rep (nth parts 4)
              :docs-count (nth parts 5)
              :docs-deleted (nth parts 6)
              :store-size (nth parts 7)
              :pri-store-size (nth parts 8))))

(defn get-indices [url]
  (let [url (string/join "/" [url "_cat" "indices"])]
    (try
      (let [res (http/get url)]
        (if (http/success? res)
          (let [lines (string/split-lines (:body res))]
            (map indices-response->map lines))
          []))
      (catch Exception e
        (log/warnf "Failed to get indices: %s" (.getMessage e))
        []))))

(defn index-exists? [url index-name]
  (let [indices (get-indices url)]
    (pos? (count (filter #(= index-name (:index %)) indices)))))

(defn search-doc-type [url index-name doc-type]
  (try
    (if (index-exists? url index-name)
      (let [search-url (string/join "/" [url index-name doc-type "_search"])
            res (http/get search-url)]
        (if (http/success? res)
          (json/parse-string (:body res))
          {}))
      {})
    (catch Exception e
      (log/warnf "Failed to search | %s" {:url url :index index-name :msg (.getMessage e)})
      {})))

(defn delete-index [url index-name]
  (try
    (if (index-exists? url index-name)
      (let [delete-url (string/join "/" [url index-name])]
        (http/success? (http/delete delete-url)))
      false)
    (catch Exception e
      (log/warnf "Failed to delete index | %s" {:url url :index index-name :msg (.getMessage e)})
      false)))

(defn index-doc-count [url index-name]
  (try
    (if (index-exists? url index-name)
      (let [count-url (string/join "/" [url index-name "_count"])
            res (http/get count-url)]
        (if (http/success? res)
          (get (json/parse-string (:body res)) "count")
          0))
      0)
    (catch Exception e
      (log/warnf "Failed to get document count | %s" {:url url :index index-name :msg (.getMessage e)})
      0)))

