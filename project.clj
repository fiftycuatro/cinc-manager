(defproject cinc-manager "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.clojure/tools.cli "0.3.3"]
                 [org.clojure/core.async "0.2.374"]
                 [com.cognitect/transit-clj "0.8.285"]
                 [clj-time "0.11.0"]
                 [clj-http "2.0.1"]
                 [ring/ring-core "1.4.0"]
                 [ring/ring-jetty-adapter "1.4.0"]
                 [ring-transit "0.1.4"]
                 [compojure/compojure "1.4.0"]
                 [com.stuartsierra/component "0.2.3"]
                 [clamq/clamq-activemq "0.4"]
                 [org.clojure/tools.logging "0.3.1"]
                 [ch.qos.logback/logback-classic "1.1.3"]
                 [org.slf4j/log4j-over-slf4j "1.7.15"]      ;; needed for ActiveMQ client logger
                 [cheshire "5.5.0"]

                 ;; cljs deps
                 [org.clojure/clojurescript "1.7.107"]
                 [com.cognitect/transit-cljs "0.8.237"]
                 [reagent "0.6.0-alpha"]
                 [devcards "0.2.1-5"]
                 [secretary "1.2.0"]
                 [com.andrewmcveigh/cljs-time "0.4.0"]
                 [org.clojars.frozenlock/reagent-modals "0.2.3"]]

  :plugins [[lein-cljsbuild "1.1.2"]]
  :main cinc-manager.core
  :source-paths ["src/clj", "src/cljs"]
  :profiles {:uberjar {:aot :all
                       :prep-tasks ["compile" ["cljsbuild" "once"]]
                       }
             :dev {:source-paths ["dev" "src/clj"]
                   :dependencies [[org.clojure/tools.namespace "0.2.3"]
                                  [org.clojure/java.classpath "0.2.0"]
                                  [figwheel-sidecar "0.5.0-SNAPSHOT"] ]}}

  :cljsbuild { :builds [{:source-paths ["src/cljs"]
                         :jar true
                         :compiler {
                                    :asset-path "cljs/"
                                    :output-to "target/classes/public/cljs/app.js"
                                    :output-dir "target/classes/public/cljs"
                                    :optimizations :whitespace
                                    :static-fns true
                                    :optimize-constants true
                                    :pretty-print true}}]})