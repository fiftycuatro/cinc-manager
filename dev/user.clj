(ns user
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.pprint :refer (pprint)]
            [clojure.repl :refer :all]
            [clojure.test :as test]
            [figwheel-sidecar.repl-api :as ra]
            [clojure.tools.namespace.repl :refer (refresh refresh-all disable-unload! disable-reload!)]
            [com.stuartsierra.component :as component]
            [cinc-manager.system :as system]))

(disable-unload!)
(disable-reload!)

(def server-config {:port 8081
                    :logsets-path "dev-resources/log-sets"
                    :logtype-config-path "dev-resources/log-types.edn"
                    :broker-url "tcp://localhost:61616"
                    :broker-dest "com.fiftycuatro.cinc.events"
                    :elasticsearch-url "http://localhost:9200"})

(defn figwheel-config [devcards?]
  {:figwheel-options { }
   :build-ids ["dev"]
   :all-builds
                     [{:id "dev"
                       :figwheel {:devcards devcards?}
                       :source-paths ["src/cljs"]
                       :compiler {:main 'cinc-manager.core
                                  :asset-path "/cljs"
                                  :output-to "target/classes/public/cljs/app.js"
                                  :output-dir "target/classes/public/cljs"
                                  :optimizations :none
                                  :static-fns true
                                  :optimize-constants true
                                  :pretty-print true
                                  :externs ["src/js/externs.js"]
                                  :closure-defines '{goog.DEBUG true}
                                  :verbose true}}]})

(defrecord Figwheel []
  component/Lifecycle
  (start [config]
    (ra/start-figwheel! config)
    config)
  (stop [config]
    (ra/stop-figwheel!)
    config))

(def sys
  (atom
    (component/system-map
      :figwheel (map->Figwheel (figwheel-config false))
      :app-server (system/dev-system server-config))))

(defn start
  ([] (start {:devcards? false}))
  ([options]
  ;; I couldn't really figure out why I
  ;; had to redefine the whole system everytime
  (swap! sys (fn [sys]
               (component/system-map
                 :figwheel (map->Figwheel (figwheel-config (:devcards? options)))
                 :app-server (system/dev-system server-config))))
  (swap! sys component/start)))

(defn start-devcards []
  (start {:devcards? true}))

(defn stop []
  (swap! sys component/stop))

(defn reset []
  (stop)
  (refresh :after 'user/start))

(defn reset-devcards []
  (stop)
  (refresh :after 'user/start-devcards))

(defn cljs-repl []
  "Start clojurescript repl"
  (ra/cljs-repl))

