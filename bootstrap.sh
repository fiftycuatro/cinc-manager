#!/usr/bin/env bash

# Add Oracle Java Repo
sudo add-apt-repository ppa:webupd8team/java  -y

# update apt
sudo apt-get update

# install java

#accept the licenses
echo debconf shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections && echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections
sudo apt-get install --with-recommends software-properties-common -y
sudo apt-get install --with-recommends oracle-java8-installer -y
sudo apt-get install oracle-java8-set-default -y

sudo adduser --home /home/elasticsearch --system --shell /bin/bash elasticsearch
sudo groupadd elasticsearch
 
JAVA_HOME=/usr/lib/jvm/java-8-oracle
echo "export JAVA_HOME=$JAVA_HOME" >> /etc/bash.bashrc 
echo "JAVA_HOME=$JAVA_HOME" >> /etc/environment


ACTIVEMQ_VERSION=5.13.0
ACTIVEMQ_PARENT=/opt/activemq
ACTIVEMQ_HOME=$ACTIVEMQ_PARENT/latest
ACTIVEMQ_BASE=$ACTIVEMQ_HOME
ACTIVEMQ_CONF=$ACTIVEMQ_HOME/conf
ACTIVEMQ_DATA=$ACTIVEMQ_HOME/data
ACTIVEMQ_TMP=$ACTIVEMQ_BASE/tmp

sudo adduser --home $ACTIVEMQ_PARENT --system --shell /bin/bash activemq

cd $ACTIVEMQ_PARENT
wget --progress=bar:force http://apache.go-parts.com/activemq/$ACTIVEMQ_VERSION/apache-activemq-$ACTIVEMQ_VERSION-bin.tar.gz
sudo tar -zxvf apache-activemq-$ACTIVEMQ_VERSION-bin.tar.gz

echo "export ACTIVEMQ_HOME=$ACTIVEMQ_HOME" >> /etc/bash.bashrc 

#add the Activemq user
sudo chown -R activemq:users $ACTIVEMQ_PARENT/apache-activemq-$ACTIVEMQ_VERSION
sudo ln -snf $ACTIVEMQ_PARENT/apache-activemq-$ACTIVEMQ_VERSION $ACTIVEMQ_HOME

sudo cp $ACTIVEMQ_HOME/bin/env /etc/default/activemq
sudo sed -i '~s/^ACTIVEMQ_USER=""/ACTIVEMQ_USER="activemq"/' /etc/default/activemq
sudo sed -i '~s/^# ACTIVEMQ_BASE=.*$/ACTIVEMQ_BASE="$ACTIVEMQ_HOME"/' /etc/default/activemq
sudo sed -i '~s/^# ACTIVEMQ_CONF=/ACTIVEMQ_CONF=/' /etc/default/activemq
sudo sed -i '~s/^# ACTIVEMQ_DATA=/ACTIVEMQ_DATA=/' /etc/default/activemq
sudo sed -i '~s/^# ACTIVEMQ_TMP=/ACTIVEMQ_TMP=/' /etc/default/activemq

sudo chmod 644 /etc/default/activemq
sudo ln -snf $ACTIVEMQ_HOME/bin/activemq /etc/init.d/activemq
sudo update-rc.d activemq defaults

sudo service activemq start


cd /home/vagrant
wget https://download.elastic.co/logstash/logstash/logstash-all-plugins-2.2.0.tar.gz 

tar -xvf logstash-all-plugins-2.2.0.tar.gz

#Install Elasticsearch
#cd /tmp
wget https://download.elasticsearch.org/elasticsearch/release/org/elasticsearch/distribution/deb/elasticsearch/2.2.0/elasticsearch-2.2.0.tar.gz
tar -xvf elasticsearch-2.2.0.tar.gz


#Install Kibana
wget https://download.elastic.co/kibana/kibana/kibana-4.4.1-linux-x64.tar.gz
tar -xvf kibana-4.4.1-linux-x64.tar.gz


